<?php
    class Model {
        private $pdo;
        public function __construct() {
            $this->pdo = new PDO('mysql:host=localhost;dbname=nobel', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => "Donnes NAMES utf8"));
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        public function get_last() {
            $sql = "SELECT * FROM nobels ORDER BY year DESC LIMIT 25";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        public function get_nb_nobel_prizes() {
            $sql = "SELECT count(*) FROM nobels";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            return $stmt->fetchColumn();
        }

?>