<?php
$servername = "localhost";
$dbname = "database_nobel";
$username = "root";
$password = '';

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
} catch (PDOException $e) {
    echo "Erreur de connexion" . $e->getMessage();
}
?>