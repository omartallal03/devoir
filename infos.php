<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php
            $stmt->execute(['id' => $id]);
            if ($stmt->rowCount() > 0) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                echo $row['NAME'];
            } else {
                echo "Identifiant invalide";
            }
        ?>
    </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>
<body>
<?php 
        require_once 'connect.php';
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $sql = "SELECT * FROM nobels WHERE id = :id";
            $stmt = $conn->prepare($sql);
        }
?>
    <?php
        
        $stmt->execute(['id' => $id]);
        
        if ($stmt->rowCount() > 0) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            echo "  <div class='container-fluid my-2'>
                        <table class='table border table-striped table-hover'>
                            <tr>
                                <th>Name</th>
                                <td>{$row['name']}</td>
                            </tr>
                            <tr>
                                <th>Category</th>
                                <td>{$row['category']}</td>
                            </tr>
                            <tr>
                                <th>Year</th>
                                <td>{$row['year']}</td>
                            </tr>
                        </table>
                    </div>";
        } else {
            echo "<div class='container-fluid my-2'>
                    <h1 class='text-center'>Identifiant invalide</h1>
                </div>";
        }

            
    ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>
</html>